

public class Rectangle {
    /*
    This is a class representing a rectangular farm.
    It has getArea() method returning area of the farm.
     */
    private double length;
    private double breadth;

    //This is the constructor for initializing the length and breadth
    Rectangle(double length, double breadth) {
        /*
        @param : length
        @param : breadth
         */
        this.length = length;
        this.breadth = breadth;
    }

    //getArea method is used to return the area of the farm.
    public double getArea() {
        return length * breadth;
    }

    public double getPerimeter() {
        return 2 * (length + breadth);
    }
}
