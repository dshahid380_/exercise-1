import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class FarmTest {

    @Test
    public void testReturnArea0WhenLengthAndBreadthIsZero() {
        Rectangle farm = new Rectangle(0, 0);
        Assert.assertEquals(0, farm.getArea(), 0);
    }

    @Test
    public void shouldReturn4WhenLengthAndBreadthIs2() {
        Rectangle farm = new Rectangle(2.0, 2.0);
        Assert.assertEquals(4.0, farm.getArea(), 0);
    }

    @Test
    public void shouldReturn8WhenLengthAndBreadthIs2() {
        Rectangle farm = new Rectangle(2.0, 2.0);
        Assert.assertEquals(8.0, farm.getPerimeter(), 0);
    }

    @Test
    public void shouldFailIfLengthIsNegative() {
        Rectangle farm = new Rectangle(-1.0, 2.0);
        if (farm.getArea() < 0)
            fail("Dimension cannot be negative");

    }

    @Test
    public void shouldFailIfBreadthIsNegative() {
        Rectangle farm = new Rectangle(1.0, -2.0);
        if (farm.getPerimeter() < 0)
            fail("Dimension cannot be negative");

    }

}